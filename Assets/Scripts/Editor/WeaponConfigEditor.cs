using UnityEditor;
using UnityEngine;
using ECS.Configs;

[CustomEditor(typeof(WeaponConfig))]
public class WeaponConfigEditor : Editor
{
    private SerializedProperty _idProp;
    private SerializedProperty _typeProp;
    private SerializedProperty _nameProp;
    private SerializedProperty _iconProp;
    private SerializedProperty _bulletConfigProp;
    private SerializedProperty _baseDamageProp;
    private SerializedProperty _baseRangeProp;
    private SerializedProperty _baseMoveSpeedProp;
    private SerializedProperty _baseAttackSpeedProp;
    private SerializedProperty _baseLifeTimeProp;
    private SerializedProperty _damageIncreaseProp;
    private SerializedProperty _rangeIncreaseProp;
    private SerializedProperty _moveSpeedIncreaseProp;
    private SerializedProperty _attackSpeedIncreaseProp;
    private SerializedProperty _lifeTimeIncreaseProp;


    public void OnEnable()
    {
        var config = target as WeaponConfig;

        _idProp = serializedObject.FindProperty(nameof(config.id));
        _typeProp = serializedObject.FindProperty(nameof(config.type));
        _nameProp = serializedObject.FindProperty(nameof(config.weaponName));
        _iconProp = serializedObject.FindProperty(nameof(config.icon));
        _baseDamageProp = serializedObject.FindProperty(nameof(config.baseDamage));
        _baseRangeProp = serializedObject.FindProperty(nameof(config.baseRange));
        _baseMoveSpeedProp = serializedObject.FindProperty(nameof(config.baseMoveSpeed));
        _baseAttackSpeedProp = serializedObject.FindProperty(nameof(config.baseAttackSpeed));
        _baseLifeTimeProp = serializedObject.FindProperty(nameof(config.baseLifeTime));
        _damageIncreaseProp = serializedObject.FindProperty(nameof(config.damageIncrease));
        _rangeIncreaseProp = serializedObject.FindProperty(nameof(config.rangeIncrease));
        _moveSpeedIncreaseProp = serializedObject.FindProperty(nameof(config.moveSpeedIncrease));
        _attackSpeedIncreaseProp = serializedObject.FindProperty(nameof(config.attackSpeedIncrease));
        _lifeTimeIncreaseProp = serializedObject.FindProperty(nameof(config.lifeTimeIncrease));
        _bulletConfigProp = serializedObject.FindProperty(nameof(config.bulletConfig));
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_idProp, new GUIContent("ID"));
        EditorGUILayout.PropertyField(_typeProp, new GUIContent("���"));
        EditorGUILayout.PropertyField(_nameProp, new GUIContent("��������"));
        EditorGUILayout.PropertyField(_iconProp, new GUIContent("������"));
        EditorGUILayout.PropertyField(_bulletConfigProp, new GUIContent("������ ����"));
        EditorGUILayout.LabelField("��������� ������� ��������");
        EditorGUILayout.PropertyField(_baseDamageProp, new GUIContent("������� ����"));
        EditorGUILayout.PropertyField(_baseRangeProp, new GUIContent("������� ��������� ����������"));
        EditorGUILayout.PropertyField(_baseMoveSpeedProp, new GUIContent("������� �������� �������� ����"));
        EditorGUILayout.PropertyField(_baseAttackSpeedProp, new GUIContent("������� �������� �����"));
        EditorGUILayout.PropertyField(_baseLifeTimeProp, new GUIContent("������� ����� ����� ����"));
        EditorGUILayout.LabelField("��������� �������� �� �������");
        EditorGUILayout.PropertyField(_damageIncreaseProp, new GUIContent("������� ����� �� �������"));
        EditorGUILayout.PropertyField(_rangeIncreaseProp, new GUIContent("������� ��������� ���������� �� �������"));
        EditorGUILayout.PropertyField(_moveSpeedIncreaseProp, new GUIContent("������� �������� �������� ���� �� �������"));
        EditorGUILayout.PropertyField(_attackSpeedIncreaseProp, new GUIContent("������� �������� ����� �� �������"));
        EditorGUILayout.PropertyField(_lifeTimeIncreaseProp, new GUIContent("������� ������� ����� ����"));

        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("���������", GUILayout.Width(150)))
            Save();
    }

    private void Save()
    {
        EditorUtility.SetDirty(serializedObject.targetObject);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
