using ECS.Systems.Combat.Unit;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

[CustomPropertyDrawer(typeof(EnemySpawnTimeSettings))]
public class EnemySpawnTimeSettingsDrawer : PropertyDrawer
{
    public override VisualElement CreatePropertyGUI(SerializedProperty property)
    {
        var container = new VisualElement();

        container.Add(new PropertyField(property.FindPropertyRelative("enemyConfig")));
        container.Add(new PropertyField(property.FindPropertyRelative("restrictedTime")));
        container.Add(new PropertyField(property.FindPropertyRelative("spawnTimeDelay")));

        return container;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var initialIndent = EditorGUI.indentLevel;
        var enemyConfigRect = new Rect(position.x, position.y, 250, position.height);
        var restrictedTimeRect = new Rect(position.x + 250 + 10, position.y, 50, position.height);
        var spawnTimeDelayRect = new Rect(position.x + 300 + 20, position.y, 50, position.height);

        EditorGUI.indentLevel = 0;
        EditorGUI.PropertyField(enemyConfigRect, property.FindPropertyRelative("enemyConfig"), GUIContent.none);
        EditorGUI.PropertyField(restrictedTimeRect, property.FindPropertyRelative("restrictedTime"), GUIContent.none);
        GUI.Label(restrictedTimeRect, new GUIContent("", "����� � ������� � ������� �������� ���� �� ����� ���������"));
        EditorGUI.PropertyField(spawnTimeDelayRect, property.FindPropertyRelative("spawnTimeDelay"), GUIContent.none);
        GUI.Label(spawnTimeDelayRect, new GUIContent("", "�������� � �������� ����� �������� ������ �����"));
        EditorGUI.indentLevel = initialIndent;
        EditorGUI.EndProperty();
    }
}
