using UnityEditor;
using UnityEngine;
using ECS.Configs;

[CustomEditor(typeof(BulletConfig))]
public class BulletConfigEditor : Editor
{
    private SerializedProperty _idProp;
    private SerializedProperty _prefabProp;

    public void OnEnable()
    {
        var config = target as BulletConfig;

        _idProp = serializedObject.FindProperty(nameof(config.id));
        _prefabProp = serializedObject.FindProperty(nameof(config.prefab));
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_idProp, new GUIContent("ID"));
        EditorGUILayout.PropertyField(_prefabProp, new GUIContent("������ ����"));

        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("���������", GUILayout.Width(150)))
            Save();
    }

    private void Save()
    {
        EditorUtility.SetDirty(serializedObject.targetObject);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
