using UnityEditor;
using UnityEngine;
using ECS.Configs;

[CustomEditor(typeof(LevelConfig))]
public class LevelConfigEditor : Editor
{
    private SerializedProperty _id;
    private SerializedProperty _survivalTimeGoal;
    private SerializedProperty _enemySpawnSettings;

    public void OnEnable()
    {
        var config = target as LevelConfig;

        _id = serializedObject.FindProperty(nameof(config.id));
        _survivalTimeGoal = serializedObject.FindProperty(nameof(config.survivalTimeGoal));
        _enemySpawnSettings = serializedObject.FindProperty(nameof(config.enemySpawnTimeSettings));
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_id, new GUIContent("ID"));
        EditorGUILayout.PropertyField(_survivalTimeGoal, new GUIContent("����� ���������", "� �������"));
        EditorGUILayout.PropertyField(_enemySpawnSettings, new GUIContent("��������� ������"));

        serializedObject.ApplyModifiedProperties();
        if (GUILayout.Button("���������", GUILayout.Width(150)))
            Save();
    }

    private void Save()
    {
        EditorUtility.SetDirty(serializedObject.targetObject);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
