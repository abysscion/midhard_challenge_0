using UnityEditor;
using UnityEngine;
using ECS.Configs;

[CustomEditor(typeof(ExpCrystalConfigDatabase))]
public class ExpCrystalConfigDatabaseEditor : Editor
{
    private SerializedProperty _configsListProperty;

    public void OnEnable()
    {
        _configsListProperty = serializedObject.FindProperty("_configs");
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("��������", GUILayout.Width(150)))
            ((ExpCrystalConfigDatabase)target).Refresh();

        serializedObject.Update();

        GUI.enabled = false;
        EditorGUILayout.PropertyField(_configsListProperty);
        GUI.enabled = true;
    }
}
