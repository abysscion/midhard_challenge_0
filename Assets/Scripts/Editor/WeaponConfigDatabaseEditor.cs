using UnityEditor;
using UnityEngine;
using ECS.Configs;

[CustomEditor(typeof(WeaponConfigDatabase))]
public class WeaponConfigDatabaseEditor : Editor
{
    private SerializedProperty _configsListProperty;

    public void OnEnable()
    {
        _configsListProperty = serializedObject.FindProperty("_configs");
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("��������", GUILayout.Width(150)))
            ((WeaponConfigDatabase)target).Refresh();

        serializedObject.Update();

        GUI.enabled = false;
        EditorGUILayout.PropertyField(_configsListProperty);
        GUI.enabled = true;
    }
}
