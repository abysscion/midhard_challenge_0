using UnityEditor;
using UnityEngine;
using ECS.Configs;

[CustomEditor(typeof(UnitConfig))]
public class UnitConfigEditor : Editor
{
    private SerializedProperty _idProp;
    private SerializedProperty _moveSpeedProp;
    private SerializedProperty _healthProp;
    private SerializedProperty _contactDamage;
    private SerializedProperty _expDropConfig;
    private SerializedProperty _prefabProp;

    public void OnEnable()
    {
        var config = target as UnitConfig;

        _idProp = serializedObject.FindProperty(nameof(config.id));
        _moveSpeedProp = serializedObject.FindProperty(nameof(config.moveSpeed));
        _healthProp = serializedObject.FindProperty(nameof(config.health));
        _contactDamage = serializedObject.FindProperty(nameof(config.contactDamage));
        _expDropConfig = serializedObject.FindProperty(nameof(config.expDropConfig));
        _prefabProp = serializedObject.FindProperty(nameof(config.prefab));
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_idProp, new GUIContent("ID"));
        EditorGUILayout.PropertyField(_moveSpeedProp, new GUIContent("��������"));
        EditorGUILayout.PropertyField(_healthProp, new GUIContent("��������"));
        EditorGUILayout.PropertyField(_contactDamage, new GUIContent("���� ������������"));
        EditorGUILayout.PropertyField(_expDropConfig, new GUIContent("������� ����� �����"));
        EditorGUILayout.PropertyField(_prefabProp, new GUIContent("������"));

        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("���������", GUILayout.Width(150)))
            Save();
    }

    private void Save()
    {
        EditorUtility.SetDirty(serializedObject.targetObject);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
