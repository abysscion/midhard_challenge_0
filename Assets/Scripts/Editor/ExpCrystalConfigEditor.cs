using UnityEditor;
using UnityEngine;
using ECS.Configs;

[CustomEditor(typeof(ExpCrystalConfig))]
public class ExpCrystalConfigEditor : Editor
{
    private SerializedProperty _idProp;
    private SerializedProperty _expGainValueProp;
    private SerializedProperty _prefabProp;

    public void OnEnable()
    {
        var config = target as ExpCrystalConfig;

        _idProp = serializedObject.FindProperty(nameof(config.id));
        _expGainValueProp = serializedObject.FindProperty(nameof(config.expGainValue));
        _prefabProp = serializedObject.FindProperty(nameof(config.prefab));
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_idProp, new GUIContent("ID"));
        EditorGUILayout.PropertyField(_expGainValueProp, new GUIContent("���������� �����"));
        EditorGUILayout.PropertyField(_prefabProp, new GUIContent("������ ��������"));

        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("���������", GUILayout.Width(150)))
            Save();
    }

    private void Save()
    {
        EditorUtility.SetDirty(serializedObject.targetObject);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
