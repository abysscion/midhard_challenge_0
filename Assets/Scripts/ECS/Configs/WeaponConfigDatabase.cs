using UnityEngine;

namespace ECS.Configs
{
    //[CreateAssetMenu(fileName = "WeaponConfigDatabase", menuName = "Databases/Create weapon config database")]
    public class WeaponConfigDatabase : ScriptableObject
    {
        public const int defaultWeaponID = 0;

        private static WeaponConfigDatabase _instance;

        [SerializeField] private WeaponConfig[] _configs;

        public static WeaponConfigDatabase Instance
        {
            get
            {
                if (!_instance)
                    Initialize();
                return _instance;
            }
        }

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            _instance = Resources.Load<WeaponConfigDatabase>("Configs/WeaponConfigDatabase");
            _instance.Refresh();
        }

        /// <summary> �������� ������ �� ����. </summary>
        /// <returns> ������, ���� �� ���� � ��. � ��������� ������ null. </returns>
        public WeaponConfig GetConfigById(int id)
        {
            foreach (var weapon in _configs)
            {
                if (weapon.id == id)
                    return weapon;
            }

            return null;
        }

        /// <summary> �������� ��� �������. </summary>
        public WeaponConfig[] GetAllConfigs()
        {
            return _configs;
        }

        /// <summary> ������������� ������ � ��. � ������ ���������� ���������� �� ID �������� ������. </summary>
        public void Refresh()
        {
            var configs = Resources.LoadAll<WeaponConfig>("Configs/Weapons");
            var dict = new System.Collections.Generic.Dictionary<int, WeaponConfig>(configs.Length);
            
            foreach (var config in configs)
            {
                if (dict.ContainsKey(config.id))
                    Debug.LogError($"ID [{config.id}] already exists in {dict[config.id].name}! Duplicant: {config.name}", config);
                else
                    dict.Add(config.id, config);
            }

            _configs = configs;
        }
    }
}
