using ECS.Systems.Combat.Unit;
using System.Collections.Generic;
using UnityEngine;

namespace ECS.Configs
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "LevelConfig", menuName = "Configs/Create level config")]
    public partial class LevelConfig : ScriptableObject
    {
        public int id;
        public float survivalTimeGoal;
        public List<EnemySpawnTimeSettings> enemySpawnTimeSettings;
    }
}
