using ECS.Systems.Combat.Weapon;
using UnityEngine;

namespace ECS.Configs
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "WeaponConfig", menuName = "Configs/Create weapon config")]
    public class WeaponConfig : ScriptableObject
    {
        public int id;
        public WeaponType type;
        /// <summary> ������� �������� ����� </summary>
        public float baseDamage;
        /// <summary> ������� �������� ��������� ���������� </summary>
        public float baseRange;
        /// <summary> ������� �������� ���������� ���� � ������� </summary>
        public float baseAttackSpeed;
        /// <summary> ������� �������� �������� ������ ���� </summary>
        public float baseMoveSpeed;
        /// <summary> ������� ������������ ������������� ���� </summary>
        public float baseLifeTime;
        /// <summary> ������� ����� �� ������� </summary>
        public float damageIncrease;
        /// <summary> ������� ��������� ���������� �� ������� </summary>
        public float rangeIncrease;
        /// <summary> ������� ���������� ���� � ������� �� ������� </summary>
        public float attackSpeedIncrease;
        /// <summary> ������� �������� ������ ���� �� ������� </summary>
        public float moveSpeedIncrease;
        /// <summary> �������� ������������ ������������� ���� �� ������� </summary>
        public float lifeTimeIncrease;
        public BulletConfig bulletConfig;
        public string weaponName;
        public Sprite icon;
    }
}
