using UnityEngine;

namespace ECS.Configs
{
    //[CreateAssetMenu(fileName = "BulletConfigDatabase", menuName = "Databases/Create bullet config database")]
    public class BulletConfigDatabase : ScriptableObject
    {
        private static BulletConfigDatabase _instance;

        [SerializeField] private BulletConfig[] _configs;

        public static BulletConfigDatabase Instance
        {
            get
            {
                if (!_instance)
                    Initialize();
                return _instance;
            }
        }

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            _instance = Resources.Load<BulletConfigDatabase>("Configs/BulletConfigDatabase");
            _instance.Refresh();
        }

        /// <summary>
        /// �������� ������ �� ����.
        /// </summary>
        /// <returns>������, ���� �� ���� � ��. � ��������� ������ null.</returns>
        public BulletConfig GetConfigById(int id)
        {
            foreach (var weapon in _configs)
            {
                if (weapon.id == id)
                    return weapon;
            }

            return null;
        }

        /// <summary>
        /// ������������� ������ � ��. � ������ ���������� ���������� �� ID �������� ������.
        /// </summary>
        /// <returns></returns>
        public void Refresh()
        {
            var configs = Resources.LoadAll<BulletConfig>("Configs/Bullets");
            var dict = new System.Collections.Generic.Dictionary<int, BulletConfig>(configs.Length);
            
            foreach (var config in configs)
            {
                if (dict.ContainsKey(config.id))
                    Debug.LogError($"ID [{config.id}] already exists in {dict[config.id].name}! Duplicant: {config.name}", config);
                else
                    dict.Add(config.id, config);
            }

            _configs = configs;
        }
    }
}
