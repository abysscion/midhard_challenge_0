using UnityEngine;

namespace ECS.Configs
{
    //[CreateAssetMenu(fileName = "ExpCrystalConfigDatabase", menuName = "Databases/Create experience crystal config database")]
    public class ExpCrystalConfigDatabase : ScriptableObject
    {
        private static ExpCrystalConfigDatabase _instance;

        [SerializeField] private ExpCrystalConfig[] _configs;

        public static ExpCrystalConfigDatabase Instance
        {
            get
            {
                if (!_instance)
                    Initialize();
                return _instance;
            }
        }

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            _instance = Resources.Load<ExpCrystalConfigDatabase>("Configs/ExpCrystalConfigDatabase");
            _instance.Refresh();
        }

        /// <summary>
        /// �������� ������ �� ����.
        /// </summary>
        /// <returns>������, ���� �� ���� � ��. � ��������� ������ null.</returns>
        public ExpCrystalConfig GetConfigById(int id)
        {
            foreach (var crystal in _configs)
            {
                if (crystal.id == id)
                    return crystal;
            }

            return null;
        }

        /// <summary>
        /// ������������� ������ � ��. � ������ ���������� ���������� �� ID �������� ������.
        /// </summary>
        /// <returns></returns>
        public void Refresh()
        {
            var configs = Resources.LoadAll<ExpCrystalConfig>("Configs/ExpCrystals");
            var dict = new System.Collections.Generic.Dictionary<int, ExpCrystalConfig>(configs.Length);
            
            foreach (var config in configs)
            {
                if (dict.ContainsKey(config.id))
                    Debug.LogError($"ID [{config.id}] already exists in {dict[config.id].name}! Duplicant: {config.name}", config);
                else
                    dict.Add(config.id, config);
            }

            _configs = configs;
        }
    }
}
