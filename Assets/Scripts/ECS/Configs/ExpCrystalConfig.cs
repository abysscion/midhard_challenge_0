using UnityEngine;

namespace ECS.Configs
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "ExpCrystalConfig", menuName = "Configs/Create experience crystal config")]
    public partial class ExpCrystalConfig : ScriptableObject
    {
        public int id;
        public float expGainValue;
        public GameObject prefab;
    }
}
