using UnityEngine;

namespace ECS.Configs
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "UnitConfig", menuName = "Configs/Create unit config")]
    public class UnitConfig : ScriptableObject
    {
        public int id;
        public float moveSpeed = 5f;
        public float health = 100f;
        public float contactDamage = 0f;
        public ExpCrystalConfig expDropConfig;
        public GameObject prefab;
    }
}
