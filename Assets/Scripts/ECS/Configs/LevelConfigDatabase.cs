using UnityEngine;

namespace ECS.Configs
{
    //[CreateAssetMenu(fileName = "LevelConfigDatabase", menuName = "Databases/Create level config database")]
    public class LevelConfigDatabase : ScriptableObject
    { 
        private static LevelConfigDatabase _instance;

        [SerializeField] private LevelConfig[] _configs;

        public static LevelConfigDatabase Instance
        {
            get
            {
                if (!_instance)
                    Initialize();
                return _instance;
            }
        }

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            _instance = Resources.Load<LevelConfigDatabase>("Configs/LevelConfigDatabase");
            _instance.Refresh();
        }

        /// <summary>
        /// �������� ������ �� ����.
        /// </summary>
        /// <returns>������, ���� �� ���� � ��. � ��������� ������ null.</returns>
        public LevelConfig GetConfigById(int id)
        {
            foreach (var unit in _configs)
            {
                if (unit.id == id)
                    return unit;
            }

            return null;
        }

        /// <summary>
        /// ������������� ������ � ��. � ������ ���������� ���������� �� ID �������� ������.
        /// </summary>
        /// <returns></returns>
        public void Refresh()
        {
            var configs = Resources.LoadAll<LevelConfig>("Configs/Levels");
            var dict = new System.Collections.Generic.Dictionary<int, LevelConfig>(configs.Length);
            
            foreach (var config in configs)
            {
                if (dict.ContainsKey(config.id))
                    Debug.LogError($"ID [{config.id}] already exists in {dict[config.id].name}! Duplicant: {config.name}", config);
                else
                    dict.Add(config.id, config);
            }

            _configs = configs;
        }
    }
}
