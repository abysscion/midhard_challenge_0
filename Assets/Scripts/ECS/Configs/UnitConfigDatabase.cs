using UnityEngine;

namespace ECS.Configs
{
    //[CreateAssetMenu(fileName = "UnitConfigDatabase", menuName = "Databases/Create unit config database")]
    public class UnitConfigDatabase : ScriptableObject
    {
        public const int playerId = 0;

        private static UnitConfigDatabase _instance;

        [SerializeField] private UnitConfig[] _configs;

        public static UnitConfigDatabase Instance
        {
            get
            {
                if (!_instance)
                    Initialize();
                return _instance;
            }
        }

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            _instance = Resources.Load<UnitConfigDatabase>("Configs/UnitConfigDatabase");
            _instance.Refresh();
        }

        /// <summary>
        /// �������� ������ �� ����.
        /// </summary>
        /// <returns>������, ���� �� ���� � ��. � ��������� ������ null.</returns>
        public UnitConfig GetConfigById(int id)
        {
            foreach (var unit in _configs)
            {
                if (unit.id == id)
                    return unit;
            }

            return null;
        }

        /// <summary>
        /// ������������� ������ � ��. � ������ ���������� ���������� �� ID �������� ������.
        /// </summary>
        /// <returns></returns>
        public void Refresh()
        {
            var configs = Resources.LoadAll<UnitConfig>("Configs/Units");
            var dict = new System.Collections.Generic.Dictionary<int, UnitConfig>(configs.Length);
            
            foreach (var config in configs)
            {
                if (dict.ContainsKey(config.id))
                    Debug.LogError($"ID [{config.id}] already exists in {dict[config.id].name}! Duplicant: {config.name}", config);
                else
                    dict.Add(config.id, config);
            }

            _configs = configs;
        }
    }
}
