using UnityEngine;

namespace ECS.Configs
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "BulletConfig", menuName = "Configs/Create bullet config")]
    public partial class BulletConfig : ScriptableObject
    {
        public int id;
        public GameObject prefab;
    }
}
