using Doozy.Engine.UI;
using ECS;
using ECS.Configs;
using ECS.Systems;
using System.Collections.Generic;
using UnityEngine;

public class WeaponViewUIHelper : MonoBehaviour
{
    [SerializeField] private UIView _view;
    [SerializeField] private EcsController _ecsController;
    [SerializeField] private WeaponChoiseUIHelper[] _weaponChoiseHelpers = new WeaponChoiseUIHelper[0];

    private SharableLevelData SharedData => _ecsController.SharableData;

    private void Start()
    {
        SharedData.OnPlayerLevelChanged += OnPlayerLevelChanged;
    }

    private void OnPlayerLevelChanged(int levelValue)
    {
        ShowView();
    }

    public void ShowView()
    {
        SetupWeaponChoiseButtons();
        _view.Show();
        Time.timeScale = 0;
    }

    private void SetupWeaponChoiseButtons()
    {
        var weaponDbInst = WeaponConfigDatabase.Instance;
        var weaponToShowCount = 3;
        var allConfigsIds = GetAllWeaponConfigsIds(weaponDbInst.GetAllConfigs());
        var playerWeapons = SharedData.GetPlayerWeapons();
        var toAddConfigsIdsPool = new List<int>();
        var randomTriesLimit = 100;

        if (allConfigsIds.Count < weaponToShowCount)
        {
            Debug.LogError($"� ���� ������ ������ ������ {weaponToShowCount} ������. ���������� ������� ������ ������ ������", gameObject);
            return;
        }

        toAddConfigsIdsPool.Add(playerWeapons[Random.Range(0, playerWeapons.Count)].configId);

        while (toAddConfigsIdsPool.Count != weaponToShowCount && randomTriesLimit > 0)
        {
            var i = Random.Range(0, allConfigsIds.Count);

            randomTriesLimit--;
            if (toAddConfigsIdsPool.Contains(allConfigsIds[i]))
            {
                allConfigsIds.RemoveAt(i);
                continue;
            }

            toAddConfigsIdsPool.Add(allConfigsIds[i]);
        }

        if (randomTriesLimit == 0)
            Debug.LogError("�������� � ��������� ���������� ������!", gameObject);

        for (var i = 0; i < weaponToShowCount; i++)
            _weaponChoiseHelpers[i].SetupButton(weaponDbInst.GetConfigById(toAddConfigsIdsPool[i]));
    }

    private List<int> GetAllWeaponConfigsIds(WeaponConfig[] configs)
    {
        var lst = new List<int>();

        foreach (var config in configs)
            lst.Add(config.id);

        return lst;
    }
}
