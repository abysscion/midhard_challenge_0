using Sirenix.OdinInspector;
using UnityEngine;

public class CanDetectCollisionAdapter : MonoBehaviour
{
    [ReadOnly] public int hostEntityId;
    /// <summary> ���������� ��� ��������� ����-�� � ������� ��������� ���������� </summary>
    public System.Action<int, int, Collider> OnEnterTrigger;
    /// <summary> ���������� ���������, ���� �������� ���������� ���������� ���-�� ���� </summary>
    public System.Action<int, int, Collider> OnStayTrigger;

    private const float TriggerStayActivationDelay = 0.25f;
    private float _lastTriggerStayActivationTime;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out CanDetectCollisionAdapter otherAdapter))
            OnEnterTrigger?.Invoke(hostEntityId, otherAdapter.hostEntityId, other);
    }

    private void OnTriggerStay(Collider other)
    {
        if (TriggerStayActivationDelay >= Time.timeSinceLevelLoad - _lastTriggerStayActivationTime)
            return;

        if (other.TryGetComponent(out CanDetectCollisionAdapter otherAdapter))
            OnStayTrigger?.Invoke(hostEntityId, otherAdapter.hostEntityId, other);
    }
}
