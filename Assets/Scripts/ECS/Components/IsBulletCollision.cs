using UnityEngine;

namespace ECS.Components
{
    public struct IsBulletCollision
    {
        public int hostEntityId;
        public int collidedEntityId;
        public Collider collider;
    }
}
