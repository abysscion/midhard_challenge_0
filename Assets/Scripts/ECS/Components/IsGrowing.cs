using UnityEngine;

namespace ECS.Components
{
    public struct IsGrowing
    {
        public Vector3 scaleChangeSpeed;
        public Vector3 scaleGrowLimit;
    }
}
