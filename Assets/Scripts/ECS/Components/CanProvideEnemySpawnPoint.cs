using UnityEngine;

namespace ECS.Components
{
    public struct CanProvideEnemySpawnPoint
    {
        private Vector3 _spawnPoint;

        public bool IsSpawnPointFresh { get; private set; }

        public void PutFreshSpawnPoint(Vector3 spawnPoint)
        {
            IsSpawnPointFresh = true;
            _spawnPoint = spawnPoint;
        }

        public bool TryTakeFreshSpawnPoint(out Vector3 spawnPoint)
        {
            if (IsSpawnPointFresh)
            {
                IsSpawnPointFresh = false;
                spawnPoint = _spawnPoint;
                return true;
            }

            spawnPoint = Vector3.zero;
            return false;
        }
    }
}
