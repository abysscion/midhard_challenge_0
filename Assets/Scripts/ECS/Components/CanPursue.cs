using UnityEngine;

namespace ECS.Components
{
    public struct CanPursue
    {
        public Transform targetTransform;
    }
}
