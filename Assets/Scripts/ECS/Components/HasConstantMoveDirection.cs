using UnityEngine;

namespace ECS.Components
{
    public struct HasConstantMoveDirection
    {
        public Vector3 direction;
    }
}
