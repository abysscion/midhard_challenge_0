using UnityEngine;

namespace ECS.Components
{
    public struct IsExpCrystalCollision
    {
        public int hostEntityId;
        public int collidedEntityId;
        public Collider collider;
    }
}
