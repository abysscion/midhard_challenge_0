namespace ECS.Components
{
    public struct IsDamageInstance
    {
        public int targetEntityId;
        public float amount;
    }
}
