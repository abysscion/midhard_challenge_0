using UnityEngine;

namespace ECS.Components
{
    public struct CanListenInput
    {
        public Vector3 moveVector;
    }
}
