using UnityEngine;

namespace ECS.Components
{
    public struct IsExpCrystalCreationRequest
    {
        public int configId;
        public Vector3 position;
    }
}
