using ECS.Systems.Combat.Weapon;
using System.Collections.Generic;

namespace ECS.Components
{
    public struct HasWeapon
    {
        public List<WeaponData> weaponDataList;
    }
}
