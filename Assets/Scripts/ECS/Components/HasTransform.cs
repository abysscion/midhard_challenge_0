using UnityEngine;

namespace ECS.Components
{
    public struct HasTransform
    {
        public Transform transform;
    }
}
