using UnityEngine;

namespace ECS.Components
{
    public struct IsEnemyCollision
    {
        public int hostEntityId;
        public int collidedEntityId;
        public Collider collider;
    }
}
