namespace ECS.Components
{
    public struct IsExpGain
    {
        public int targetEntityId;
        public float amount;
    }
}
