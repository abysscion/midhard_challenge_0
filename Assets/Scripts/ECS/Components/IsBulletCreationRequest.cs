using ECS.Systems.Combat.Weapon;
using UnityEngine;

namespace ECS.Components
{
    public struct IsBulletCreationRequest
    {
        public WeaponData weaponData;
        public Transform target;
    }
}
