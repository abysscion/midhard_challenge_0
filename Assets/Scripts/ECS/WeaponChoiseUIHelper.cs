using ECS;
using ECS.Configs;
using ECS.Systems;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WeaponChoiseUIHelper : MonoBehaviour
{
    [SerializeField] private EcsController _ecsController;

    [SerializeField] private TMP_Text _weaponName;
    [SerializeField] private TMP_Text _lvlTxt;
    [SerializeField] private Image _weaponIcon;

    private (int id, int level) _currentOnClickSettingsForWeapon;

    private SharableLevelData SharedData => _ecsController.SharableData;

    public void SetupButton(WeaponConfig config)
    {
        var isWeaponAdded = SharedData.TryGetPlayerWeaponLevel(config.id, out var currentWeaponLvl);
        var desiredWeaponLvl = isWeaponAdded ? currentWeaponLvl + 1 : 0;

        _lvlTxt.text = isWeaponAdded ? $"LVL {currentWeaponLvl} + 1" : "LVL 0";
        _weaponIcon.sprite = config.icon;
        _weaponName.text = config.weaponName;
        _currentOnClickSettingsForWeapon = (config.id, desiredWeaponLvl);
    }

    public void OnClick_AddOrUpgradeWeapon()
    {
        SharedData.SetPlayerWeaponLevel(_currentOnClickSettingsForWeapon.id, _currentOnClickSettingsForWeapon.level);
    }
}
