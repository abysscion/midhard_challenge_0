using ECS.Systems.Combat;
using ECS.Systems.Combat.Bullet;
using ECS.Systems.Combat.Experience;
using ECS.Systems.Combat.Unit;
using ECS.Systems.Combat.Weapon;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECS.Systems
{
    public class EcsController : MonoBehaviour
    {
        [SerializeField] private SharableLevelData _sharableData;

        private EcsWorld _world;
        private EcsSystems _initSystems;
        private EcsSystems _updateSystems;

        public SharableLevelData SharableData => _sharableData;

        private void Start()
        {
            _world = new EcsWorld();

            _initSystems = new EcsSystems(_world, _sharableData);
            _initSystems
                .Add(new PlayerIniter())
                .Init();

            _updateSystems = new EcsSystems(_world, _sharableData);
            _updateSystems
                .Add(new EnemySpawnPointProvider())
                .Add(new EnemySpawner())
                .Add(new InputListener())
                .Add(new PlayerMover())
                .Add(new ConstantDirectionMover())
                .Add(new Grower())
                .Add(new Pursuer())
                .Add(new WeaponUsageHandler())
                .Add(new BulletCreator())
                .Add(new BulletCollisionHandler())
                .Add(new EnemyCollisionHandler())
                .Add(new ExpCrystalCollisionHandler())
                .Add(new ExpGainHandler())
                .Add(new DamageApplier())
                .Add(new EnemyHealthHandler())
                .Add(new PlayerHealthHandler())
                .Add(new ExpCrystalCreator())
                .Add(new DiedEntitiesCleaner())
                .Init();
        }

        private void Update()
        {
            _updateSystems.Run();
        }

        private void OnDestroy()
        {
            _initSystems?.Destroy();
            _initSystems = null;
            _updateSystems?.Destroy();
            _updateSystems = null;
            _world?.Destroy();
        }
    }
}
