using ECS.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECS.Systems
{
    public class Grower : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _growFilter;
        private EcsPool<IsGrowing> _growPool;
        private EcsPool<HasTransform> _tfPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _growFilter = _world.Filter<IsGrowing>().Inc<HasTransform>().End();
            _growPool = _world.GetPool<IsGrowing>();
            _tfPool = _world.GetPool<HasTransform>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var growerEntityId in _growFilter)
            {
                ref var collisionComponent = ref _growPool.Get(growerEntityId);
                ref var tf = ref _tfPool.Get(growerEntityId);
                var prevScale = tf.transform.localScale;
                var addScale = collisionComponent.scaleChangeSpeed * Time.deltaTime;
                var scaleChangeLeft = new Vector3(Mathf.Max(collisionComponent.scaleGrowLimit.x - addScale.x, 0f),
                                                  Mathf.Max(collisionComponent.scaleGrowLimit.y - addScale.y, 0f),
                                                  Mathf.Max(collisionComponent.scaleGrowLimit.z - addScale.z, 0f));
                var resultScale = new Vector3(Mathf.Min(prevScale.x + addScale.x, collisionComponent.scaleGrowLimit.x),
                                              Mathf.Min(prevScale.y + addScale.y, collisionComponent.scaleGrowLimit.y),
                                              Mathf.Min(prevScale.z + addScale.z, collisionComponent.scaleGrowLimit.z));

                tf.transform.localScale = resultScale;
                if (scaleChangeLeft.x == 0f || scaleChangeLeft.y == 0f || scaleChangeLeft.z == 0f)
                    _growPool.Del(growerEntityId);
            }
        }
    }
}
