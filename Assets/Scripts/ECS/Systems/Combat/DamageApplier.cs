﻿using ECS.Components;
using Leopotam.EcsLite;

namespace ECS.Systems.Combat
{
    class DamageApplier : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _damageFilter;
        private EcsPool<IsDamageInstance> _damagePool;
        private EcsPool<HasHealth> _healthPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _damageFilter = _world.Filter<IsDamageInstance>().End();
            _damagePool = _world.GetPool<IsDamageInstance>();
            _healthPool = _world.GetPool<HasHealth>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var damageEntityId in _damageFilter)
            {
                ref var damage = ref _damagePool.Get(damageEntityId);

                if (_healthPool.Has(damage.targetEntityId))
                {
                    ref var health = ref _healthPool.Get(damage.targetEntityId);
                    health.amount -= damage.amount;
                }

                _world.DelEntity(damageEntityId);
            }
        }
    }
}
