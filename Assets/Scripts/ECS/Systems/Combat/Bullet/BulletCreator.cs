using Cysharp.Threading.Tasks;
using ECS.Components;
using ECS.Systems.Combat.Weapon;
using Leopotam.EcsLite;
using System.Threading;
using UnityEngine;

namespace ECS.Systems.Combat.Bullet
{
    public class BulletCreator : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _requestsFilter;
        private EcsPool<IsBulletCreationRequest> _requestsPool;
        private SharableLevelData _sharedData;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _requestsFilter = _world.Filter<IsBulletCreationRequest>().End();
            _requestsPool = _world.GetPool<IsBulletCreationRequest>();
            _sharedData = systems.GetShared<SharableLevelData>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _requestsFilter)
            {
                ref var request = ref _requestsPool.Get(entity);

                switch (request.weaponData.config.type)
                {
                    case WeaponType.Default:
                        CreateAndSetupBulletEntity(request.weaponData, request.target.position);
                        break;
                    case WeaponType.Spread:
                        CreateAndSetupSpreadBulletsEntities(request.weaponData, request.target.position);
                        break;
                    case WeaponType.Growing:
                        CreateAndSetupGrowerBulletEntity(request.weaponData, request.target.position);
                        break;
                    case WeaponType.Scatter:
                        CreateAndSetupScatterBulletEntity(request.weaponData, request.target.position);
                        break;
                }

                _world.DelEntity(entity);
            }
        }

        private int CreateAndSetupBulletEntity(WeaponData weaponData, Vector3 targetPos)
        {
            var bulletEntityId = _world.NewEntity();

            ref var tf = ref _world.GetPool<HasTransform>().Add(bulletEntityId);
            ref var move = ref _world.GetPool<CanMove>().Add(bulletEntityId);
            ref var moveDirection = ref _world.GetPool<HasConstantMoveDirection>().Add(bulletEntityId);
            _world.GetPool<IsBullet>().Add(bulletEntityId);
            _world.GetPool<CanDetectCollision>().Add(bulletEntityId);

            var bulletConfig = weaponData.config.bulletConfig;
            var bulletObj = GameObject.Instantiate(bulletConfig.prefab, _sharedData.playerTf.position,
                                                   Quaternion.identity, _sharedData.bulletsRootObject);
            var adapter = GetOrCreateCollisionAdapter(bulletObj);
            var cst = new CancellationTokenSource();

            tf.transform = bulletObj.transform;
            move.speed = weaponData.MoveSpeed;
            moveDirection.direction = (targetPos - bulletObj.transform.position).normalized;
            adapter.hostEntityId = bulletEntityId;
            adapter.OnEnterTrigger += (hostEntityId, collidedEntityId, collider) => CreateCollisionEntity(hostEntityId, collidedEntityId, collider, cst);
            adapter.OnEnterTrigger += (__, collidedEntityId, _) => CreateDamageEntity(weaponData, collidedEntityId);
            DeleteBulletAfterLifetimeEnds(bulletEntityId, bulletObj, weaponData.LifeTime, cst);

            return bulletEntityId;
        }

        private CanDetectCollisionAdapter GetOrCreateCollisionAdapter(GameObject gameObject)
        {
            if (!gameObject.TryGetComponent(out CanDetectCollisionAdapter adapter))
                adapter = gameObject.AddComponent<CanDetectCollisionAdapter>();

            return adapter;
        }

        private void CreateCollisionEntity(int hostEntityId, int collidedEntityId, Collider collider, CancellationTokenSource cst)
        {
            ref var collisionComponent = ref _world.GetPool<IsBulletCollision>().Add(_world.NewEntity());

            collisionComponent.hostEntityId = hostEntityId;
            collisionComponent.collidedEntityId = collidedEntityId;
            collisionComponent.collider = collider;
            cst.Cancel();
        }

        private void CreateDamageEntity(WeaponData weaponData, int collidedEntityId)
        {
            ref var damageInstance = ref _world.GetPool<IsDamageInstance>().Add(_world.NewEntity());

            damageInstance.amount = weaponData.Damage;
            damageInstance.targetEntityId = collidedEntityId;
        }

        //todo: ������� ��������� ��������� ������������ �� �������
        private async void DeleteBulletAfterLifetimeEnds(int bulletEntityId, GameObject bulletGameObject, float lifeTime, CancellationTokenSource cst)
        {
            await UniTask.Delay((int)(lifeTime * 1000), cancellationToken: cst.Token).SuppressCancellationThrow();
            _world.DelEntity(bulletEntityId);
            GameObject.Destroy(bulletGameObject);
        }

        private int[] CreateAndSetupSpreadBulletsEntities(WeaponData weaponData, Vector3 targetPos)
        {
            var bulletsCount = 8;
            var createdEntities = new int[bulletsCount];

            for (var i = 0; i < bulletsCount; i++)
            {
                createdEntities[i] = CreateAndSetupBulletEntity(weaponData, targetPos);
                ref var move = ref _world.GetPool<HasConstantMoveDirection>().Get(createdEntities[i]);
                move.direction = Quaternion.Euler(new Vector3(0, Random.Range(-15f, 15f), 0)) * move.direction;
            }

            return createdEntities;
        }

        private int CreateAndSetupGrowerBulletEntity(WeaponData weaponData, Vector3 targetPos)
        {
            var bulletEntityId = CreateAndSetupBulletEntity(weaponData, targetPos);
            ref var move = ref _world.GetPool<HasConstantMoveDirection>().Get(bulletEntityId);
            ref var grow = ref _world.GetPool<IsGrowing>().Add(bulletEntityId);

            grow.scaleChangeSpeed = new Vector3(2, 2, 2);
            grow.scaleGrowLimit = new Vector3(3, 3, 3);
            return bulletEntityId;
        }

        private int CreateAndSetupScatterBulletEntity(WeaponData weaponData, Vector3 targetPos)
        {
            var bulletEntityId = CreateAndSetupBulletEntity(weaponData, targetPos);
            var adapter = GetOrCreateCollisionAdapter(_world.GetPool<HasTransform>().Get(bulletEntityId).transform.gameObject);

            adapter.OnEnterTrigger += (_, __, collider) => HelpSpawnScatterBullet(weaponData, Vector3.forward, collider.transform.position);

            return bulletEntityId;
        }

        private async void HelpSpawnScatterBullet(WeaponData weaponData, Vector3 targetPos, Vector3 startPos)
        {
            for (var i = 0; i < 8; i++)
            {
                await UniTask.DelayFrame(1);

                var createdEntityId = CreateAndSetupBulletEntity(weaponData, targetPos);

                _world.GetPool<HasTransform>().Get(createdEntityId).transform.localScale *= 0.5f;
                _world.GetPool<HasTransform>().Get(createdEntityId).transform.position = startPos;
                _world.GetPool<HasConstantMoveDirection>().Get(createdEntityId).direction = Quaternion.Euler(new Vector3(0, 45 * i, 0)) * _world.GetPool<HasConstantMoveDirection>().Get(createdEntityId).direction;
            }
        }
    }
}
