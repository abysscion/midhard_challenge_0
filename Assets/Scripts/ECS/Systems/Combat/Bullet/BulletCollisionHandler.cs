using ECS.Components;
using Leopotam.EcsLite;

namespace ECS.Systems.Combat.Bullet
{
    public class BulletCollisionHandler : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _collisionsFilter;
        private EcsPool<IsBulletCollision> _collisionsPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _collisionsFilter = _world.Filter<IsBulletCollision>().End();
            _collisionsPool = _world.GetPool<IsBulletCollision>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var collisionEntityId in _collisionsFilter)
            {
                ref var collisionComponent = ref _collisionsPool.Get(collisionEntityId);

                _world.DelEntity(collisionEntityId);
            }
        }
    }
}
