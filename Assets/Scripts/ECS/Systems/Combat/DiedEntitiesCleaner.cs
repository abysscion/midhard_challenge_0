﻿using ECS.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECS.Systems.Combat
{
    class DiedEntitiesCleaner : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _justDeadFilter;
        private EcsFilter _deadWithTfFilter;
        private EcsPool<HasTransform> _deadWithTfPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _justDeadFilter = _world.Filter<MarkedAsDead>().End();
            _deadWithTfFilter = _world.Filter<MarkedAsDead>().Inc<HasTransform>().End();
            _deadWithTfPool = _world.GetPool<HasTransform>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var deadWithTfEntityId in _deadWithTfFilter)
            {
                ref var tf = ref _deadWithTfPool.Get(deadWithTfEntityId);

                GameObject.Destroy(tf.transform.gameObject);
                _world.DelEntity(deadWithTfEntityId);
            }

            foreach (var deadEntityId in _justDeadFilter)
                _world.DelEntity(deadEntityId);
        }
    }
}
