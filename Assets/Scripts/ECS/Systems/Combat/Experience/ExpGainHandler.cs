using ECS.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECS.Systems.Combat.Unit
{
    public class ExpGainHandler : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _expfilter;
        private EcsPool<IsExpGain> _expPool;
        private SharableLevelData _sharedData;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _expfilter = _world.Filter<IsExpGain>().End();
            _expPool = _world.GetPool<IsExpGain>();
            _sharedData = systems.GetShared<SharableLevelData>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var expEntityId in _expfilter)
            {
                ref var expGain = ref _expPool.Get(expEntityId);

                _sharedData.PlayerCurrentExperience += expGain.amount;
                _world.DelEntity(expEntityId);
            }
        }
    }
}
