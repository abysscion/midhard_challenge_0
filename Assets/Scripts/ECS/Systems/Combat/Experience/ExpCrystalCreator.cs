using ECS.Components;
using ECS.Configs;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECS.Systems.Combat.Experience
{
    public class ExpCrystalCreator : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _requestsFilter;
        private EcsPool<IsExpCrystalCreationRequest> _requestsPool;
        private EcsPool<CanDetectCollision> _collisionPool;
        private EcsPool<IsExpCrystal> _expCrystalPool;
        private SharableLevelData _sharedData;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _requestsFilter = _world.Filter<IsExpCrystalCreationRequest>().End();
            _requestsPool = _world.GetPool<IsExpCrystalCreationRequest>();
            _collisionPool = _world.GetPool<CanDetectCollision>();
            _expCrystalPool = _world.GetPool<IsExpCrystal>();
            _sharedData = systems.GetShared<SharableLevelData>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var requestEntityId in _requestsFilter)
            {
                ref var request = ref _requestsPool.Get(requestEntityId);

                CreateAndSetupExpCrystalEntity(request.configId, request.position);
                _world.DelEntity(requestEntityId);
            }
        }

        private void CreateAndSetupExpCrystalEntity(int expCrystalConfigId, Vector3 position)
        {
            var expCrystalEntityId = _world.NewEntity();

            ref var tf = ref _world.GetPool<HasTransform>().Add(expCrystalEntityId);
            _collisionPool.Add(expCrystalEntityId);
            _expCrystalPool.Add(expCrystalEntityId);

            var expCrystalConfig = ExpCrystalConfigDatabase.Instance.GetConfigById(expCrystalConfigId);
            var expCrystalObj = GameObject.Instantiate(expCrystalConfig.prefab, position,
                                                   Quaternion.identity, _sharedData.expCrystalsRootObject);
            var adapter = GetOrCreateCollisionAdapter(expCrystalObj);

            tf.transform = expCrystalObj.transform;
            adapter.hostEntityId = expCrystalEntityId;
            adapter.OnEnterTrigger += (hostEntityId, collidedEntityId, collider) => CreateCollisionEntity(hostEntityId, collidedEntityId, collider);
            adapter.OnEnterTrigger += (_, collidedEntityId, __) => CreateExpGainEntity(collidedEntityId, expCrystalConfig.expGainValue);
        }

        private CanDetectCollisionAdapter GetOrCreateCollisionAdapter(GameObject gameObject)
        {
            if (!gameObject.TryGetComponent(out CanDetectCollisionAdapter adapter))
                adapter = gameObject.AddComponent<CanDetectCollisionAdapter>();

            return adapter;
        }

        private void CreateCollisionEntity(int hostEntityId, int collidedEntityId, Collider collider)
        {
            ref var collisionComponent = ref _world.GetPool<IsExpCrystalCollision>().Add(_world.NewEntity());

            collisionComponent.hostEntityId = hostEntityId;
            collisionComponent.collidedEntityId = collidedEntityId;
            collisionComponent.collider = collider;
        }

        private void CreateExpGainEntity(int targetEntityId, float amount)
        {
            ref var expGain = ref _world.GetPool<IsExpGain>().Add(_world.NewEntity());

            expGain.targetEntityId = targetEntityId;
            expGain.amount = amount;
        }
    }
}
