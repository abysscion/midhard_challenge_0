using ECS.Components;
using Leopotam.EcsLite;

namespace ECS.Systems.Combat.Bullet
{
    public class ExpCrystalCollisionHandler : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _collisionsFilter;
        private EcsPool<IsExpCrystalCollision> _collisionsPool;
        private EcsPool<MarkedAsDead> _deathMarkPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _collisionsFilter = _world.Filter<IsExpCrystalCollision>().End();
            _collisionsPool = _world.GetPool<IsExpCrystalCollision>();
            _deathMarkPool = _world.GetPool<MarkedAsDead>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var collisionEntityId in _collisionsFilter)
            {
                ref var collisionComponent = ref _collisionsPool.Get(collisionEntityId);

                _deathMarkPool.Add(collisionComponent.hostEntityId);
                _world.DelEntity(collisionEntityId);
            }
        }
    }
}
