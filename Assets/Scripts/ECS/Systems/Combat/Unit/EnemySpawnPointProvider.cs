using ECS.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECS.Systems.Combat.Unit
{
    public class EnemySpawnPointProvider : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private SharableLevelData _sharedData;
        private Camera _cam;
        private EcsFilter _filter;
        private EcsPool<CanProvideEnemySpawnPoint> _providerPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _sharedData = systems.GetShared<SharableLevelData>();
            _cam = _sharedData.camera;
            _filter = _world.Filter<CanProvideEnemySpawnPoint>().End();
            _providerPool = _world.GetPool<CanProvideEnemySpawnPoint>();

            ref var component = ref _providerPool.Add(_world.NewEntity());
            component.PutFreshSpawnPoint(GenerateSpawnPoint());
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var component = ref _providerPool.Get(entity);

                if (!component.IsSpawnPointFresh)
                    component.PutFreshSpawnPoint(GenerateSpawnPoint());
            }
        }

        private Vector3 GenerateSpawnPoint()
        {
            var zPos = _cam.transform.position.z;
            var floorHeight = _sharedData.floor.transform.position.y;
            //����� ���������� �������� ������, ���� ����������� � ��� ���� � ������� ������� ������������ ��������
            var camFrustrumPoint0 = GetPointAtHeight(_cam.ViewportPointToRay(new Vector3(0, 0, -zPos)), floorHeight);
            var camFrustrumPoint1 = GetPointAtHeight(_cam.ViewportPointToRay(new Vector3(1, 0, -zPos)), floorHeight);
            var camFrustrumPoint2 = GetPointAtHeight(_cam.ViewportPointToRay(new Vector3(0, 1, -zPos)), floorHeight);
            var camFrustrumPoint3 = GetPointAtHeight(_cam.ViewportPointToRay(new Vector3(1, 1, -zPos)), floorHeight);
            //������� ����� �������� �������� ����� ���������� ������ ����������. � ������ ����� ���������� ������������� ��� ��� ���� :^)
            var newFrustrumPoint0 = camFrustrumPoint0 + new Vector3(-10, 0, -10);  //todo: �������� ����� ���������� ��� ��������
            var newFrustrumPoint1 = camFrustrumPoint1 + new Vector3(10, 0, -10);   //tmp
            var newFrustrumPoint2 = camFrustrumPoint2 + new Vector3(-18, 0, 10);   //tmp
            var newFrustrumPoint3 = camFrustrumPoint3 + new Vector3(18, 0, 10);    //tmp
            //����� �������� �� ��� ������������
            var triangle0 = new Triangle { p0 = camFrustrumPoint0, p1 = camFrustrumPoint1, p2 = camFrustrumPoint2 };
            var triangle1 = new Triangle { p0 = camFrustrumPoint3, p1 = camFrustrumPoint1, p2 = camFrustrumPoint2 };
            //�������� ������������� �������� 100 ����� ����� ������ ��������
            for (var randomTriesLimit = 100; randomTriesLimit >= 0; randomTriesLimit--)
            {
                var point = GenerateRandomPointAlongFrustrumSides(newFrustrumPoint0, newFrustrumPoint1, newFrustrumPoint2, newFrustrumPoint3);

                //����� ������ ����� ����, ��� ������ ��������� ����� �������� � ��������� ����� �������������� ������ ��������
                if (!triangle0.IsPointInside(point) && !triangle1.IsPointInside(point))
                    return point;
            }

            //����� 100 ��������� ������� ���������� ����� ������ ���� ��������
            return newFrustrumPoint0;
        }

        private static Vector3 GetPointAtHeight(Ray ray, float height)
        {
            var distanceToPoint = (height - ray.origin.y) / ray.direction.y;

            return ray.origin + distanceToPoint * ray.direction;
        }

        //���������� ��������� ����� ����� ����� �� ������ ��������.
        //��� ������������ ������ ���������� ���������
        //��� �������������� ������ �������� � ��������� ������ ���, �.�. ��� ��������� ������� min/max �� X/Z.
        //  � Z ���������� � ������ ������ �������������� ������ ����������.
        //  ���������� ��������� ����� � ��������������, �������� ���������� (�������������� �������� ��������)
        //  ��-�������� ����� ��������� �� ���-�� ����� ���������� � ����� ������ ����������
        private Vector3 GenerateRandomPointAlongFrustrumSides(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            var floorHeight = 0; //_sharedData.floor.transform.position.y;

            return (Random.Range(0, 4)) switch
            {
                0 => new Vector3(Random.Range(p2.x, p3.x), floorHeight, Random.Range(p2.z, p3.z)),
                1 => new Vector3(Random.Range(p0.x, p1.x), floorHeight, Random.Range(p0.z, p1.z)),
                2 => new Vector3(Random.Range(p0.x, p2.x), floorHeight, Random.Range(p0.z, p2.z)),
                3 => new Vector3(Random.Range(p1.x, p3.x), floorHeight, Random.Range(p1.z, p3.z)),
                _ => p0,
            };
        }

        private class Triangle
        {
            public Vector3 p0, p1, p2;

            public bool IsPointInside(Vector3 p)
            {
                var s = (p0.x - p2.x) * (p.z - p2.z) - (p0.z - p2.z) * (p.x - p2.x);
                var t = (p1.x - p0.x) * (p.z - p0.z) - (p1.z - p0.z) * (p.x - p0.x);

                if ((s < 0) != (t < 0) && s != 0 && t != 0)
                    return false;

                var d = (p2.x - p1.x) * (p.z - p1.z) - (p2.z - p1.z) * (p.x - p1.x);
                return d == 0 || (d < 0) == (s + t <= 0);
            }
        }
    }
}
