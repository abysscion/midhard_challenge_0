using ECS.Components;
using ECS.Configs;
using Leopotam.EcsLite;
using System.Collections.Generic;
using UnityEngine;

namespace ECS.Systems.Combat.Unit
{
    public class EnemySpawner : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private SharableLevelData _sharedData;
        private LevelConfig _levelConfig;
        private List<SpawnSettingsData> _spawnSettings;
        private EcsFilter _spawnPointProvidersFilter;
        private EcsPool<CanProvideEnemySpawnPoint> _spawnPointProviderPool;
        private EcsPool<IsEnemy> _enemyPool;
        private EcsPool<HasTransform> _tfPool;
        private EcsPool<HasHealth> _healthPool;
        private EcsPool<CanPursue> _pursuePool;
        private EcsPool<CanMove> _movePool;
        private EcsPool<CanDropExpCrystal> _expDropPool;
        private EcsPool<CanDetectCollision> _collisionDetectPool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _sharedData = systems.GetShared<SharableLevelData>();
            _levelConfig = LevelConfigDatabase.Instance.GetConfigById(_sharedData.levelID);
            _spawnSettings = new List<SpawnSettingsData>();
            _spawnPointProvidersFilter = _world.Filter<CanProvideEnemySpawnPoint>().End();
            _spawnPointProviderPool = _world.GetPool<CanProvideEnemySpawnPoint>();
            _enemyPool = _world.GetPool<IsEnemy>();
            _tfPool = _world.GetPool<HasTransform>();
            _healthPool = _world.GetPool<HasHealth>();
            _pursuePool = _world.GetPool<CanPursue>();
            _movePool = _world.GetPool<CanMove>();
            _expDropPool = _world.GetPool<CanDropExpCrystal>();
            _collisionDetectPool = _world.GetPool<CanDetectCollision>();

            foreach (var settings in _levelConfig.enemySpawnTimeSettings)
                _spawnSettings.Add(new SpawnSettingsData(settings.enemyConfig, settings.restrictedTime, settings.spawnTimeDelay));
        }

        public void Run(EcsSystems systems)
        {
            foreach (var settings in _spawnSettings)
                TrySpawnEnemy(settings);
        }

        private void TrySpawnEnemy(SpawnSettingsData spawnSettings)
        {
            if (!SpawnConditionsMet(spawnSettings))
                return;

            var enemyEntityId = _world.NewEntity();

            _enemyPool.Add(enemyEntityId);
            ref var tf = ref _tfPool.Add(enemyEntityId);
            ref var health = ref _healthPool.Add(enemyEntityId);
            ref var pursue = ref _pursuePool.Add(enemyEntityId);
            ref var move = ref _movePool.Add(enemyEntityId);
            ref var expDrop = ref _expDropPool.Add(enemyEntityId);
            _collisionDetectPool.Add(enemyEntityId);

            var config = spawnSettings.enemyConfig;
            var spawnedObj = GameObject.Instantiate(config.prefab, GetSpawnPoint(), Quaternion.identity, _sharedData.enemiesRootObject);
            var adapter = GetOrCreateCollisionAdapter(spawnedObj);

            tf.transform = spawnedObj.transform;
            pursue.targetTransform = _sharedData.playerTf;
            move.speed = config.moveSpeed;
            health.amount = config.health;
            expDrop.expCrystalConfigId = config.expDropConfig.id;
            adapter.hostEntityId = enemyEntityId;
            adapter.OnStayTrigger += (_, collidedEntityId, __) => CreateDamageEntity(config.contactDamage, collidedEntityId);
            spawnSettings.lastSpawnTime = Time.timeSinceLevelLoad;
        }

        private bool SpawnConditionsMet(SpawnSettingsData spawnSettings)
        {
            if (spawnSettings.restrictedTime > Time.timeSinceLevelLoad)
                return false;
            if (spawnSettings.spawnDelay > Time.timeSinceLevelLoad - spawnSettings.lastSpawnTime)
                return false;

            return true;
        }

        private Vector3 GetSpawnPoint()
        {
            foreach (var entity in _spawnPointProvidersFilter)
            {
                ref var component = ref _spawnPointProviderPool.Get(entity);

                if (component.TryTakeFreshSpawnPoint(out var spawnPoint))
                    return spawnPoint;
            }

            return Vector3.zero;
        }

        private CanDetectCollisionAdapter GetOrCreateCollisionAdapter(GameObject gameObject)
        {
            if (!gameObject.TryGetComponent(out CanDetectCollisionAdapter adapter))
                adapter = gameObject.AddComponent<CanDetectCollisionAdapter>();

            return adapter;
        }

        private void CreateDamageEntity(float amount, int targetEntityId)
        {
            if (_world.GetPool<IsBullet>().Has(targetEntityId))
                return;

            ref var damageInstance = ref _world.GetPool<IsDamageInstance>().Add(_world.NewEntity());

            damageInstance.amount = amount;
            damageInstance.targetEntityId = targetEntityId;
        }

        private class SpawnSettingsData
        {
            public UnitConfig enemyConfig;
            public float restrictedTime;
            public float spawnDelay;
            public float lastSpawnTime = 0;

            public SpawnSettingsData(UnitConfig enemyConfig, float restrictedTime, float spawnDelay)
            {
                this.enemyConfig = enemyConfig;
                this.restrictedTime = restrictedTime * 60; //�.�. ����������� ����� ������ �������������� � �������
                this.spawnDelay = spawnDelay;
            }
        }
    }
}
