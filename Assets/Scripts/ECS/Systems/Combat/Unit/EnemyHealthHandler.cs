﻿using ECS.Components;
using Leopotam.EcsLite;

namespace ECS.Systems.Combat.Unit
{
    class EnemyHealthHandler : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _enemyFilter;
        private EcsPool<HasHealth> _healthPool;
        private EcsPool<HasTransform> _tfPool;
        private EcsPool<CanDropExpCrystal> _expDropPool;
        private EcsPool<IsExpCrystalCreationRequest> _requestPool;
        private SharableLevelData _sharedData;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _enemyFilter = _world.Filter<IsEnemy>()
                                 .Inc<HasHealth>()
                                 .Inc<HasTransform>()
                                 .End();
            _healthPool = _world.GetPool<HasHealth>();
            _tfPool = _world.GetPool<HasTransform>();
            _expDropPool = _world.GetPool<CanDropExpCrystal>();
            _requestPool = _world.GetPool<IsExpCrystalCreationRequest>();
            _sharedData = systems.GetShared<SharableLevelData>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var enemyEntityId in _enemyFilter)
            {
                ref var health = ref _healthPool.Get(enemyEntityId);

                if (health.amount > 0)
                    continue;

                if (_expDropPool.Has(enemyEntityId))
                {
                    ref var dropper = ref _expDropPool.Get(enemyEntityId);
                    ref var tf = ref _tfPool.Get(enemyEntityId);
                    ref var request = ref _requestPool.Add(_world.NewEntity());

                    request.configId = dropper.expCrystalConfigId;
                    request.position = tf.transform.position;
                    _expDropPool.Del(enemyEntityId);
                }

                _sharedData.EnemyKillCounter++;
                _world.GetPool<MarkedAsDead>().Add(enemyEntityId);
            }
        }
    }
}
