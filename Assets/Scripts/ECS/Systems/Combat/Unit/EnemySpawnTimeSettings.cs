﻿using ECS.Configs;
using System;

namespace ECS.Systems.Combat.Unit
{
    [Serializable]
    public class EnemySpawnTimeSettings
    {
        public UnitConfig enemyConfig;
        /// <summary> Время в минутах в течение которого враг не будет спавнится </summary>
        public float restrictedTime;
        /// <summary> Задержка в секундах между спавнами нового врага </summary>
        public float spawnTimeDelay;
    }
}
