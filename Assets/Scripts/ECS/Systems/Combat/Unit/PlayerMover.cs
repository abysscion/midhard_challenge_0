using ECS.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECS.Systems.Combat.Unit
{
    public class PlayerMover : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _playerFilter;
        private EcsPool<CanMove> _movePool;
        private EcsPool<CanListenInput> _inputListenPool;
        private SharableLevelData _sharedData;
        private Quaternion _cameraRotation;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _playerFilter = _world.Filter<CanMove>().Inc<CanListenInput>().End();
            _movePool = _world.GetPool<CanMove>();
            _inputListenPool = _world.GetPool<CanListenInput>();
            _sharedData = systems.GetShared<SharableLevelData>();
            _cameraRotation = Quaternion.Euler(0, _sharedData.camera.transform.rotation.eulerAngles.y, 0);
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _playerFilter)
            {
                ref var input = ref _inputListenPool.Get(entity);
                ref var move = ref _movePool.Get(entity);

                _sharedData.playerTf.position += (_cameraRotation * input.moveVector).normalized * move.speed * Time.deltaTime;
            }
        }
    }
}
