﻿using ECS.Components;
using Leopotam.EcsLite;

namespace ECS.Systems.Combat.Unit
{
    class PlayerHealthHandler : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _enemyFilter;
        private EcsPool<HasHealth> _healthPool;
        private SharableLevelData _sharedData;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _enemyFilter = _world.Filter<CanListenInput>()
                                 .Inc<HasHealth>()
                                 .Inc<HasTransform>()
                                 .End();
            _healthPool = _world.GetPool<HasHealth>();
            _sharedData = systems.GetShared<SharableLevelData>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var enemyEntityId in _enemyFilter)
            {
                ref var health = ref _healthPool.Get(enemyEntityId);

                _sharedData.PlayerHealth = health.amount;
            }
        }
    }
}
