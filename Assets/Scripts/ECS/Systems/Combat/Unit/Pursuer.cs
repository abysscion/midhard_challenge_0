using Cysharp.Threading.Tasks;
using ECS.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECS.Systems.Combat.Unit
{
    public class Pursuer : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _filter;
        private Vector3 _tmpVec;
        private EcsPool<HasTransform> _tfPool;
        private EcsPool<CanPursue> _pursuePool;
        private EcsPool<CanMove> _movePool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<CanPursue>().Inc<HasTransform>().Inc<CanMove>().End();
            _tfPool = _world.GetPool<HasTransform>();
            _pursuePool = _world.GetPool<CanPursue>();
            _movePool = _world.GetPool<CanMove>();
        }

        public void Run(EcsSystems systems)
        {
            PursueTarget(systems);
        }

        private void PursueTarget(EcsSystems systems)
        {
            foreach (var entityId in _filter)
            {
                ref var tf = ref _tfPool.Get(entityId);
                ref var move = ref _movePool.Get(entityId);
                ref var pursue = ref _pursuePool.Get(entityId);
                
                if (pursue.targetTransform == null)
                {
                    _world.GetPool<CanPursue>().Del(entityId);
                    Debug.LogWarning("��� ������ � ��������� ���������� ��������� �������������?", tf.transform.gameObject);
                    DeleteEntity(5000, entityId, tf.transform.gameObject);
                    return;
                }

                _tmpVec = (pursue.targetTransform.position - tf.transform.position).normalized;
                tf.transform.position += _tmpVec * move.speed * Time.deltaTime;
            }
        }

        private async void DeleteEntity(int msDelay, int entityId, GameObject gameObject)
        {
            await UniTask.Delay(msDelay);
            _world.DelEntity(entityId);
            GameObject.Destroy(gameObject);
        }
    }
}
