using ECS.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECS.Systems.Combat.Unit
{
    public class ConstantDirectionMover : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _filter;
        private EcsPool<HasConstantMoveDirection> _moveDirPool;
        private EcsPool<HasTransform> _tfPool;
        private EcsPool<CanMove> _movePool;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<HasConstantMoveDirection>().Inc<HasTransform>().Inc<CanMove>().End();
            _moveDirPool = _world.GetPool<HasConstantMoveDirection>();
            _tfPool =  _world.GetPool<HasTransform>();
            _movePool = _world.GetPool<CanMove>();
        }

        public void Run(EcsSystems systems)
        {
            ApplyMovement();
        }

        private void ApplyMovement()
        {
            foreach (var entity in _filter)
            {
                ref var moveDirection = ref _moveDirPool.Get(entity);
                ref var tf = ref _tfPool.Get(entity);
                ref var move = ref _movePool.Get(entity);

                tf.transform.position += moveDirection.direction * move.speed * Time.deltaTime;
            }
        }
    }
}
