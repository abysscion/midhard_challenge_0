﻿namespace ECS.Systems.Combat.Weapon
{
    public enum WeaponType
    {
        Default = 1,
        Spread = 2,
        Growing = 4,
        Scatter = 8
    }
}
