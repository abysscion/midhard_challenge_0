using ECS.Components;
using ECS.Configs;
using Leopotam.EcsLite;
using System.Collections.Generic;
using UnityEngine;

namespace ECS.Systems.Combat.Weapon
{
    public class WeaponUsageHandler : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _weaponFilter;
        private SharableLevelData _sharedData;
        private List<WeaponUsageData> _weaponsUsageData;
        private EcsPool<HasWeapon> _weaponPool;
        private EcsPool<HasTransform> _tfPool;
        private EcsPool<IsBulletCreationRequest> _requestPool;
        private Collider[] _collidersBuffer;
        private LayerMask _enemyLayerMask;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _sharedData = systems.GetShared<SharableLevelData>();
            _weaponFilter = _world.Filter<HasWeapon>().Inc<HasTransform>().End();
            _collidersBuffer = new Collider[100];
            _enemyLayerMask = LayerMask.GetMask("Enemy");
            _weaponsUsageData = new List<WeaponUsageData>();
            _tfPool = _world.GetPool<HasTransform>();
            _requestPool = _world.GetPool<IsBulletCreationRequest>();
            _sharedData.OnPlayerWeaponAdded += OnPlayerWeaponAdded;
            _sharedData.OnPlayerWeaponLevelChanged += OnPlayerWeaponChanged;
            _weaponPool = _world.GetPool<HasWeapon>();

            foreach (var hasWeaponEntityId in _weaponFilter)
            {
                ref var weaponComponent = ref _weaponPool.Get(hasWeaponEntityId);

                foreach (var weaponData in weaponComponent.weaponDataList)
                    _weaponsUsageData.Add(new WeaponUsageData(hasWeaponEntityId, weaponData));
            }
        }

        public void Run(EcsSystems systems)
        {
            foreach (var weapon in _weaponsUsageData)
                TryUseWeapon(weapon);
        }

        private void TryUseWeapon(WeaponUsageData usageData)
        {
            if (!UsageConditionsMet(usageData))
                return;

            var ownerTf = _tfPool.Get(usageData.ownerEntity).transform;
            var closestEnemyTf = GetClosestEnemyTransform(ownerTf, usageData.weaponData.Range);

            if (closestEnemyTf)
                CreateBulletRequestEntity(usageData.weaponData, closestEnemyTf);
            usageData.lastUseTime = Time.timeSinceLevelLoad;
        }

        private bool UsageConditionsMet(WeaponUsageData usageData)
        {
            if (usageData.weaponData.UsageDelay > Time.timeSinceLevelLoad - usageData.lastUseTime)
                return false;

            return true;
        }

        private Transform GetClosestEnemyTransform(Transform hostTf, float range)
        {
            var closestEnemyIndex = -1;
            var smallestDistance = Mathf.Infinity;

            System.Array.Clear(_collidersBuffer, 0, _collidersBuffer.Length - 1);
            Physics.OverlapSphereNonAlloc(hostTf.position, range, _collidersBuffer, _enemyLayerMask);

            for (int i = 0; i < _collidersBuffer.Length; i++)
            {
                if (!_collidersBuffer[i])
                    continue;

                var tmpDistance = (_collidersBuffer[i].transform.position - hostTf.position).magnitude;

                if (tmpDistance < smallestDistance)
                {
                    smallestDistance = tmpDistance;
                    closestEnemyIndex = i;
                }
            }

            return closestEnemyIndex != -1 ? _collidersBuffer[closestEnemyIndex].transform : null;
        }

        private void CreateBulletRequestEntity(WeaponData weaponData, Transform enemyTf)
        {
            ref var request = ref _requestPool.Add(_world.NewEntity());

            request.weaponData = weaponData;
            request.target = enemyTf;
        }

        private void OnPlayerWeaponChanged(int weaponConfigId, int desiredLvl)
        {
            ref var playerHasWeaponComponent = ref _weaponPool.Get(UnitConfigDatabase.playerId);

            foreach (var weaponData in playerHasWeaponComponent.weaponDataList)
            {
                if (weaponData.config.id == weaponConfigId)
                    weaponData.currentLevel = desiredLvl;
            }
        }

        private void OnPlayerWeaponAdded(int weaponConfigId)
        {
            ref var playerHasWeaponComponent = ref _weaponPool.Get(UnitConfigDatabase.playerId);
            var newWeaponData = new WeaponData(WeaponConfigDatabase.Instance.GetConfigById(weaponConfigId));

            _weaponsUsageData.Add(new WeaponUsageData(UnitConfigDatabase.playerId, newWeaponData));
            playerHasWeaponComponent.weaponDataList.Add(newWeaponData);
        }

        private class WeaponUsageData
        {
            public int ownerEntity;
            public WeaponData weaponData;
            public float lastUseTime = 0;

            public WeaponUsageData(int ownerEntity, WeaponData data)
            {
                this.ownerEntity = ownerEntity;
                this.weaponData = data;
                lastUseTime = data.UsageDelay;
            }
        }
    }
}
