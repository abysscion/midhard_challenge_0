using ECS.Configs;

namespace ECS.Systems.Combat.Weapon
{
    [System.Serializable]
    public class WeaponData
    {
        public WeaponConfig config;
        public int currentLevel = 0;

        /// <summary> �������� ���� � ������ �������� </summary>
        public float Damage => config.baseDamage + currentLevel * config.damageIncrease;
        /// <summary> ��������� ���������� ������ </summary>
        public float Range => config.baseRange + currentLevel * config.rangeIncrease;
        /// <summary> �������� ����� ����������� � �������� </summary>
        public float UsageDelay => 1f / (config.baseAttackSpeed + currentLevel * config.attackSpeedIncrease);
        /// <summary> �������� �������� ���� </summary>
        public float MoveSpeed => config.baseMoveSpeed + currentLevel * config.moveSpeedIncrease;
        /// <summary> ����� ����� ���� � �������� </summary>
        public float LifeTime => config.baseLifeTime + currentLevel * config.lifeTimeIncrease;

        public WeaponData(WeaponConfig config)
        {
            this.config = config;
        }
    }
}
