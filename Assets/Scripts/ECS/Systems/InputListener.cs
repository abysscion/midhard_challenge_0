using ECS.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECS.Systems
{
    public class InputListener : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter _filter;
        private EcsPool<CanListenInput> _inputListenPool;
        private Vector3 _tmpVector;

        public void Init(EcsSystems systems)
        {
            _world = systems.GetWorld();
            _filter = _world.Filter<CanListenInput>().End();
            _inputListenPool = _world.GetPool<CanListenInput>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter)
            {
                ref var input = ref _inputListenPool.Get(entity);

                _tmpVector = Vector3.zero;
                _tmpVector.x = Input.GetAxisRaw("Horizontal");
                _tmpVector.z = Input.GetAxisRaw("Vertical");
                input.moveVector = _tmpVector;
            }
        }
    }
}
