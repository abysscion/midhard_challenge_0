using ECS.Components;
using ECS.Configs;
using ECS.Systems.Combat.Weapon;
using Leopotam.EcsLite;
using System.Collections.Generic;
using UnityEngine;

namespace ECS.Systems
{
    public class PlayerIniter : IEcsInitSystem
    {
        public void Init(EcsSystems systems)
        {
            InitEntity(systems);
        }

        private void InitEntity(EcsSystems systems)
        {
            var world = systems.GetWorld();
            var entity = world.NewEntity();
            ref var tf = ref world.GetPool<HasTransform>().Add(entity);
            ref var health = ref world.GetPool<HasHealth>().Add(entity);
            ref var expGain = ref world.GetPool<CanGetExpGain>().Add(entity);
            ref var input = ref world.GetPool<CanListenInput>().Add(entity);
            ref var move = ref world.GetPool<CanMove>().Add(entity);
            ref var weapons = ref world.GetPool<HasWeapon>().Add(entity);
            var playerConfig = UnitConfigDatabase.Instance.GetConfigById(UnitConfigDatabase.playerId);
            var defaultWeaponConfig = WeaponConfigDatabase.Instance.GetConfigById(WeaponConfigDatabase.defaultWeaponID);
            var sharedData = systems.GetShared<SharableLevelData>();

            tf.transform = systems.GetShared<SharableLevelData>().playerTf;
            input.moveVector = Vector3.zero;
            move.speed = playerConfig.moveSpeed;
            health.amount = playerConfig.health;
            weapons.weaponDataList = new List<WeaponData>() { new WeaponData(defaultWeaponConfig) };
            sharedData.playerInitialHealth = playerConfig.health;
            sharedData.SetPlayerWeaponLevel(defaultWeaponConfig.id, 0);
            ////test
            //var config1 = WeaponConfigDatabase.Instance.GetConfigById(1);
            //weapons.weaponsList.Add(new WeaponData(config1));
            //var config2 = WeaponConfigDatabase.Instance.GetConfigById(2);
            //weapons.weaponsList.Add(new WeaponData(config2));
            //var config3 = WeaponConfigDatabase.Instance.GetConfigById(3);
            //weapons.weaponsList.Add(new WeaponData(config3));
        }
    }
}
