using ECS.Configs;
using System.Collections.Generic;
using UnityEngine;

namespace ECS
{
    [System.Serializable]
    public class SharableLevelData
    {
        public int levelID;
        public float playerInitialHealth;
        public Transform floor;
        public Transform playerTf;
        public Transform enemiesRootObject;
        public Transform bulletsRootObject;
        public Transform expCrystalsRootObject;
        public Camera camera;

        public System.Action<int> OnPlayerLevelChanged;
        public System.Action<float, float> OnPlayerExperienceChanged;
        public System.Action<int> OnEnemyKillCounterChanged;
        public System.Action<float, float> OnPlayerHealthChanged;
        public System.Action<int, int> OnPlayerWeaponLevelChanged;
        public System.Action<int> OnPlayerWeaponAdded;

        private float _playerExperience = 0f;
        private float _playerHealth = 1;
        private int _playerLevel = 0;
        private int _enemyKillCounter;
        private Dictionary<int, int> _playerWeaponIdsToLevels = new Dictionary<int, int>();

        public float PlayerExperienceNeedToLevelUp => 200;
        public int PlayerLevel
        {
            get => _playerLevel;
            set
            {
                _playerLevel = value;
                OnPlayerLevelChanged?.Invoke(PlayerLevel);
            }
        }
        public float PlayerCurrentExperience
        {
            get => _playerExperience;
            set
            {
                _playerExperience = value;
                if (_playerExperience >= PlayerExperienceNeedToLevelUp)
                {
                    PlayerCurrentExperience = _playerExperience - PlayerExperienceNeedToLevelUp;
                    PlayerLevel++;
                }
                OnPlayerExperienceChanged?.Invoke(_playerExperience, PlayerExperienceNeedToLevelUp);
            }
        }
        public int EnemyKillCounter
        {
            get => _enemyKillCounter;
            set
            {
                _enemyKillCounter = value;
                OnEnemyKillCounterChanged?.Invoke(_enemyKillCounter);
            }
        }
        public float PlayerHealth
        {
            get => _playerHealth;
            set
            {
                _playerHealth = value;
                OnPlayerHealthChanged?.Invoke(_playerHealth, playerInitialHealth);
            }
        }
    
        public List<(int configId, int level)> GetPlayerWeapons()
        {
            var result = new List<(int, int)>();

            foreach (var weaponId in _playerWeaponIdsToLevels.Keys)
                result.Add((weaponId, _playerWeaponIdsToLevels[weaponId]));
            return result;
        }

        public bool TryGetPlayerWeaponLevel(int weaponConfigId, out int actualLevel)
        {
            return _playerWeaponIdsToLevels.TryGetValue(weaponConfigId, out actualLevel);
        }

        public void SetPlayerWeaponLevel(int configId, int desiredWeaponLevel)
        {
            if (!_playerWeaponIdsToLevels.TryGetValue(configId, out var actualWeaponLevel))
            {
                _playerWeaponIdsToLevels.Add(configId, 0);
                OnPlayerWeaponAdded?.Invoke(configId);
            }
            else
            {
                _playerWeaponIdsToLevels[configId] = desiredWeaponLevel;
                OnPlayerWeaponLevelChanged?.Invoke(configId, _playerWeaponIdsToLevels[configId]);
            }
        }
    }
}
