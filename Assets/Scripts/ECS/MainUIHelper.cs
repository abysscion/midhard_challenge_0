using ECS;
using ECS.Systems;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainUIHelper : MonoBehaviour
{
    [SerializeField] private EcsController _ecsController;

    [SerializeField] private TMP_Text _killCounterTxt;
    [SerializeField] private TMP_Text _timeCounterTxt;
    [SerializeField] private TMP_Text _lvlCounterTxt;
    [SerializeField] private Image _expLvlImage;
    [SerializeField] private Image _healthImage;
    [SerializeField] private RectTransform _hpBarRT;
    [SerializeField] private Vector3 _hpBarOffest = new Vector3(0, 5, 0);

    private SharableLevelData _sharedData;

    private void Start()
    {
        _sharedData = _ecsController.SharableData;
        _sharedData.OnEnemyKillCounterChanged += (kills) => _killCounterTxt.text = kills.ToString();
        _sharedData.OnPlayerExperienceChanged += (curExp, maxExp) => _expLvlImage.fillAmount = curExp / maxExp;
        _sharedData.OnPlayerLevelChanged += (lvl) => _lvlCounterTxt.text = $"LVL {lvl}";
        _sharedData.OnPlayerHealthChanged += (curHP, maxHP) => _healthImage.fillAmount = curHP / maxHP;
        _sharedData.OnPlayerHealthChanged += (curHP, maxHP) => { if (curHP <= 0) SceneManager.LoadScene(3); };
    }

    private void Update()
    {
        var time = Time.timeSinceLevelLoad;

        _timeCounterTxt.text = $"{string.Format("{0:00}:{1:00}", Mathf.FloorToInt(time / 60), Mathf.FloorToInt(time % 60))}";
        _hpBarRT.position = _sharedData.camera.WorldToScreenPoint(_sharedData.playerTf.position + _hpBarOffest);
    }

    public void TogglePause(bool shouldPause)
    {
        Time.timeScale = shouldPause ? 0 : 1;
    }
}
